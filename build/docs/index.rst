==================================
Mozilla Build System Documentation
==================================

Overview
========

.. toctree::
   :maxdepth: 1

   glossary

Important Concepts
==================
.. toctree::
   :maxdepth: 1

   Mozconfig Files <mozconfigs>
   Profile Guided Optimization <pgo>

mozbuild
========

mozbuild is a Python package containing a lot of the code for the
Mozilla build system.

.. toctree::
   :maxdepth: 1

   mozbuild/index
   mozbuild/frontend
   mozbuild/dumbmake


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

